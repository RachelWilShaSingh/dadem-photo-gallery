# Install python-resize-image lib
# https://pypi.org/project/python-resize-image/

from PIL import Image
from resizeimage import resizeimage
from os import listdir
from os.path import isfile, join

def ResizeImage( path, filename, prefix, width, height ):
    print( "Resize image \t Path: " + path + ", Filename: " + filename )
    fd_img = open( path + filename, 'r' )
    img = Image.open( fd_img )
    img = resizeimage.resize_height( img, height )
    img.save( path + prefix + "_" + filename , img.format )
    fd_img.close()
    
def ResizeToThumbnail( path, filename, prefix, width, height ):
    print( "Resize image \t Path: " + path + ", Filename: " + filename )
    fd_img = open( path + filename, 'r' )
    img = Image.open( fd_img )
    img = resizeimage.resize_thumbnail( img, [width, height] )
    img.save( path + prefix + "_" + filename , img.format )
    fd_img.close()
    
directory = raw_input( "Directory to create thumbnails for: " )
prefix = raw_input( "Prefix to append: " )
width = int( input( "Width of thumbnails: " ) )
height = int( input( "Height of thumbnails: " ) )
isThumbnail = raw_input( "Is thumbnail? (y/n): " )

if ( isThumbnail == "y" ) :
    isThumbnail = True
else:
    isThumbnail = False

onlyfiles = [f for f in listdir( directory ) if isfile( join( directory, f ) ) ]

for f in onlyfiles:
    if ( ".jpg" in f or ".jpeg" in f or ".JPG" in f ):
        if ( isThumbnail ):
            ResizeToThumbnail( directory, f, prefix, width, height )
        else:
            ResizeImage( directory, f, prefix, width, height )
